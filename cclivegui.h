#ifndef CCLIVEGUI_H
#define CCLIVEGUI_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class CcliveGUI; }
QT_END_NAMESPACE

class QProcess;

class CcliveGUI : public QMainWindow
{
    Q_OBJECT

public:
    CcliveGUI(QWidget *parent = nullptr);
    ~CcliveGUI();

    void getUrl();
    QStringList listUrls;

private slots:
    void on_btnDownload_clicked();
    void on_btnAppend_clicked();
    void on_btnRemove_clicked();

private:
    Ui::CcliveGUI *ui;
    QProcess *process;
};
#endif // CCLIVEGUI_H
