# Cclive GUI

Pequeña aplicación que permite utilizar cclive sin depender de una terminal, facilitando al usuario el uso del mismo. Descarga videos de YouTube o webs similares utilizando cclive en un entorno gráfico desarrolado con Qt.


### Pre-requisitos 📋

Para poder usar Cclive GUI es necesario tener instalado el paquete cclive.

### Instalación 🔧

Puedes utilizar el script install o compilarlo tu mismo con CMake. Las intrucciones a continuación:

### Instalación por Script

```
./instalar
```
### Compilar manualmente

```
mkdir build
cd build
cmake ..
make
```
Con esto ya tendrás el ejecutable listo. Podrás abrir la aplicación haciéndo doble clic sobre el archivo "cclive-gui" o con "./cclive-gui" desde una terminal.

## Construido con 🛠️

* [C++](https://isocpp.org/) - El lenguaje utilizado para el desarrollo
* [Qt5](https://www.qt.io/) - El framework utilizado para la parte gráfica de NoHAN
* [Qt Creator](https://www.qt.io/) - El ID utilizado para el desarrollo del proyecto.
* [Vim](https://www.vim.org/) - El editor de código utilizado para algunas ediciones de archivos .ui

## Autores ✒️

* **Jonathan Córdova** - *Desarrollo y documentación* - [jona3717](https://gitlab.com/jona3717)

## Licencia 📄

Este proyecto está bajo la Licencia (GPLv3) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

## Expresiones de Gratitud 🎁

* Comenta a otros sobre este proyecto 📢
* Invita una cerveza 🍺 al desarrollador. 
* Da las gracias públicamente 🤓.
* etc.



---
