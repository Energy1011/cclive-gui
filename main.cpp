#include "cclivegui.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    CcliveGUI w;
    w.show();
    return a.exec();
}
